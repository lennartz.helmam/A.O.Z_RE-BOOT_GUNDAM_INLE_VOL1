\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{translation}[2020/07/17 Translations Document Class]

\LoadClass{article}

\RequirePackage{fontspec}
\RequirePackage{indentfirst}
\RequirePackage{newunicodechar}
\RequirePackage{multicol}
\RequirePackage{xeCJK}
\RequirePackage{ruby}
\RequirePackage[margin=1in]{geometry}

\setCJKmainfont{meiryo.ttc}
\newfontfamily\meiryo{meiryo.ttc}
\newfontfamily\greekfont{OldStandard-Regular.otf}

\setlength{\leftskip}{5mm}
\setlength{\parindent}{-5mm}
\setlength{\parskip}{12pt}
\setcounter{secnumdepth}{0}
\renewcommand{\rubysize}{0.7}

\newunicodechar{♥}{{\meiryo♥}}
\newunicodechar{♡}{{\meiryo♡}}
\newunicodechar{★}{{\meiryo★}}
\newunicodechar{☆}{{\meiryo☆}}
\newunicodechar{●}{{\meiryo●}}
\newunicodechar{―}{{\meiryo―}}
\newunicodechar{①}{{\meiryo①}}
\newunicodechar{②}{{\meiryo②}}
\newunicodechar{③}{{\meiryo③}}
\newunicodechar{④}{{\meiryo④}}
\newunicodechar{μ}{{\greekfont μ}}

\makeatletter
\renewcommand{\@maketitle}{\begin{center}{\LARGE \@title \par}\end{center}}
\makeatother
